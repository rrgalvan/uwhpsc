==========================================================
Listado de ficheros contenidos UWHPSC
==========================================================

Esto es un listado de los ficheros contenidos en el repositorio_ (en
el día 16/3/2015). En principio, la idea es traducirlos todos ellos, o
al menos la mayor parte de ellos (los que creamos más interesantes). Además, si vamos a incluir paralelización con C/C++, habrá que añadir secciones adicionales.

Varios
======
- about.rst (traducido por Dani)
- aws.rst
- biblio.rst
- class_format.rst
- index.rst
- memory.rst
- metrics.rst
- random.rst
- outline.rst
- smc.rst
- software_carpentry.rst
- software_installation.rst
- toc_condensed.rst

Python
======
- animation.rst
- ipython_notebook.rst
- nose.rst
- numerical_python.rst
- python_and_fortran.rst
- python_debugging.rst
- python_functions.rst
- python_plotting.rst
- python.rst
- python_scripts_modules.rst
- python_strings.rst
- sage.rst

Control de versiones
====================
- bitbucket.rst
- git_more.rst
- git.rst
- versioncontrol.rst

Informática
===========
- computer_arch.rst
- computing_options.rst
- editors.rst
- makefiles.rst
- punchcard.rst
- reproducibility.rst
- shells.rst
- special_functions.rst
- sphinx.rst
- ssh.rst
- timing.rst
- vm.rst

Fortran
=======
- fortran_arrays.rst
- fortran_debugging.rst
- fortran_io.rst
- fortran_modules.rst
- fortran_newton.rst
- fortran.rst
- fortran_sub.rst
- fortran_taylor.rst
- gfortran_flags.rst
- project_hints.rst
- project.rst

Homework
========
- homework1.rst
- homework2.rst
- homework3.rst
- homework4.rst
- homework4_solution.rst
- homeworks.rst
- lapack_examples.rst
- linalg.rst

Paralelización
==============
- jacobi1d_mpi.rst
- jacobi1d_omp1.rst
- jacobi1d_omp2.rst
- mpi.rst
- openmp.rst
- poisson.rst

Unix
====
- top.rst
- unix.rst

.. _repositorio: https://bitbucket.org/rrgalvan/uwhpsc
